<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Blinova_HelloWorld</title>
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>
    <header>
        <img src="images/maxresdefault.jpg" alt="logo" width="300px">
        <p>Самостоятельная работа "Hello, World!"</p>
    </header>

    <main>
    <?php
        echo'Hello, World!';
    ?>
    </main>

    <footer>
        <p>Задание для самостоятельной работы</p>
        <p>Блинова Александра 211-323</p>
        <p>08.03.2022</p>
    </footer>
</body>
</html>