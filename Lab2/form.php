<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FeedbackFormBlinova</title>
    <link rel="stylesheet" href="form.css">
</head>
<body>
    <header>
        <img src="images/maxresdefault.jpg" alt="Logo" width="200">
        <h2>Самостоятельная работа "Feedback form"</p>
    </header>

    <main>
        <form action="https://httpbin.org/post" method="post">
            <label>
                <span>Имя пользователя:</span>
                <input type="text" name="name" required>
            </label>
            
            <label>
                <span>E-mail пользователя:</span>
                <input type="email" name="e-mail" required>
            </label>

            <label>
                <span>Тип обращения:</span>
                <select name="type of request" required>
                    <option value="complaint">Жалоба</option>
                    <option value="proposal">Предложение</option>
                    <option value="gratitude">Благодарность</option>
                </select>
            </label>

            <label>
                <span>Текст обращения:</span>
                <input type="text" name="text of request" required>
            </label>

            <div class="">
                <span>Вариант ответа:</span>

                <label>
                    <span>СМС</span>
                    <input type="checkbox" name="request" value="sms">
                </label>
                
                <label>
                    <span>E-mail</span>
                    <input type="checkbox" name="request" value="e-mail">
                </label>
            </div>

            <button type="submit">Отправить</button>
        </form>

        <a href="get.php">Следующая страница</a>
    </main>

    <footer>
        <p>Блинова Александра 211-323</p>
        <p>29.05.2022</p>
    </footer>
</body>
</html>